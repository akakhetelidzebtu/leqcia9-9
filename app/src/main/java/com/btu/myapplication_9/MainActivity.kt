package com.btu.myapplication_9

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import androidx.core.content.res.ResourcesCompat
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        d("activitylifecycle","onCreateActivity1")

        button.setOnClickListener {
            button.text = "Hello"
        }

    }

    override fun onStart() {
        super.onStart()
        d("activitylifecycle","onStartActivity1")

    }

    override fun onResume() {
        super.onResume()
        d("activitylifecycle","onResumeActivity1")
    }

    override fun onPause() {
        super.onPause()
        d("activitylifecycle","onPauseActivity1")
    }

    override fun onStop() {
        super.onStop()
        d("activitylifecycle","onStopActivity1")
    }

    override fun onDestroy() {
        super.onDestroy()
        d("activitylifecycle","onDestroyActivity1")
    }
}